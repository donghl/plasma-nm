# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-nm package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2018, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-nm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-01 23:30+0000\n"
"PO-Revision-Date: 2021-06-26 22:28+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#: package/contents/ui/ConnectDialog.qml:136
#, kde-format
msgid "Invalid input."
msgstr "Felaktig indata."

#: package/contents/ui/ConnectionItemDelegate.qml:108
#, kde-format
msgid "Connect to"
msgstr "Anslut till"

#: package/contents/ui/main.qml:82
#, kde-format
msgid "Wi-Fi is disabled"
msgstr "WIFI är inaktiverat"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Enable"
msgstr "Aktivera"

#: package/contents/ui/main.qml:96
#, kde-format
msgid "Disable Wi-Fi"
msgstr "Inaktivera WIFI"

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Add Custom Connection"
msgstr "Lägg till egen anslutning"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Show Saved Connections"
msgstr "Visa sparade anslutningar"

#: package/contents/ui/NetworkSettings.qml:14
#, kde-format
msgid "Add new Connection"
msgstr "Lägg till ny anslutning"

#: package/contents/ui/NetworkSettings.qml:34
#, kde-format
msgid "Save"
msgstr "Spara"

#: package/contents/ui/NetworkSettings.qml:44
#, kde-format
msgid "General"
msgstr "Allmänt"

#: package/contents/ui/NetworkSettings.qml:49
#, kde-format
msgid "SSID:"
msgstr "SSID:"

#: package/contents/ui/NetworkSettings.qml:58
#, kde-format
msgid "Hidden Network:"
msgstr "Dolt nätverk"

#: package/contents/ui/NetworkSettings.qml:64
#, kde-format
msgid "Security"
msgstr "Säkerhet"

#: package/contents/ui/NetworkSettings.qml:70
#, kde-format
msgid "Security type:"
msgstr "Säkerhetstyp:"

#: package/contents/ui/NetworkSettings.qml:79
#, kde-format
msgid "None"
msgstr "Ingen"

#: package/contents/ui/NetworkSettings.qml:80
#, kde-format
msgid "WEP Key"
msgstr "WEP-nyckel"

#: package/contents/ui/NetworkSettings.qml:81
#, kde-format
msgid "Dynamic WEP"
msgstr "Dynamisk WEP"

#: package/contents/ui/NetworkSettings.qml:82
#, kde-format
msgid "WPA/WPA2 Personal"
msgstr "WPA/WPA2 personlig"

#: package/contents/ui/NetworkSettings.qml:83
#, kde-format
msgid "WPA/WPA2 Enterprise"
msgstr "WPA/WPA2 företag"

#: package/contents/ui/NetworkSettings.qml:108
#, kde-format
msgid "Password:"
msgstr "Lösenord:"

#: package/contents/ui/NetworkSettings.qml:116
#, kde-format
msgid "Authentication:"
msgstr "Behörighetskontroll:"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "TLS"
msgstr "TLS"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "LEAP"
msgstr "LEAP"

#: package/contents/ui/NetworkSettings.qml:119
#, kde-format
msgid "FAST"
msgstr "FAST"

#: package/contents/ui/NetworkSettings.qml:120
#, kde-format
msgid "Tunneled TLS"
msgstr " Tunnel-TLS"

#: package/contents/ui/NetworkSettings.qml:121
#, kde-format
msgid "Protected EAP"
msgstr " Skyddad EAP"

#: package/contents/ui/NetworkSettings.qml:130
#, kde-format
msgid "IP settings"
msgstr "IP-inställningar"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Automatic"
msgstr "Automatisk"

#: package/contents/ui/NetworkSettings.qml:136
#, kde-format
msgid "Manual"
msgstr "Manuell"

#: package/contents/ui/NetworkSettings.qml:146
#, kde-format
msgid "IP Address:"
msgstr "IP-adress:"

#: package/contents/ui/NetworkSettings.qml:158
#, kde-format
msgid "Gateway:"
msgstr "Förmedlingsnod:"

#: package/contents/ui/NetworkSettings.qml:170
#, kde-format
msgid "Network prefix length:"
msgstr "Längd på nätverksprefix:"

#: package/contents/ui/NetworkSettings.qml:183
#, kde-format
msgid "DNS:"
msgstr "DNS:"

#: package/contents/ui/PasswordField.qml:13
#, kde-format
msgid "Password…"
msgstr "Lösenord…"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Wi-Fi networks"
#~ msgstr "WIFI-nätverk"

#~ msgid "Martin Kacej"
#~ msgstr "Martin Kacej"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Avbryt"

#~ msgctxt "@action:button"
#~ msgid "Done"
#~ msgstr "Klar"

#~ msgid "Password..."
#~ msgstr "Lösenord..."

#~ msgid "Create Hotspot"
#~ msgstr "Skapa accesspunkt"

#~ msgid "Wi-Fi Hotspot"
#~ msgstr "WIFI-accesspunkt"

#~ msgid "Configure"
#~ msgstr "Anpassa"

#~ msgid "SSID"
#~ msgstr "SSID"

#~ msgid "My Hotspot"
#~ msgstr "Min accesspunkt"

#~ msgid "Hide this network"
#~ msgstr "Dölj nätverket"

#~ msgid "Protect hotspot with WPA2/PSK password"
#~ msgstr "Skydda accesspunkt med WPA2/PSK-lösenord"

#~ msgid "Save Hotspot configuration"
#~ msgstr "Spara inställning av accesspunkt"

#~ msgid "Disable Wi-Fi Hotspot"
#~ msgstr "Inaktivera WIFI-accesspunkt"

#~ msgid "Enable Wi-Fi Hotspot"
#~ msgstr "Aktivera WIFI-accesspunkt"

#~ msgid "Hotspot is inactive"
#~ msgstr "Accesspunkt är inaktiv"

#~ msgid "Not possible to start Access point."
#~ msgstr "Inte möjligt att starta åtkomstpunkt"

#~ msgid "Access point running: %1"
#~ msgstr "Åtkomstpunkt aktiv: %1"

#~ msgid "No suitable configuration found."
#~ msgstr "Ingen lämplig inställning hittades."

#~ msgid "Access point available: %1"
#~ msgstr "Åtkomstpunkt tillgänglig: %1"

#~ msgid "Connection Name"
#~ msgstr "Anslutningsnamn"

#~ msgid "(Unchanged)"
#~ msgstr "(oförändrad)"

#~ msgid "Delete connection %1 from device?"
#~ msgstr "Ta bort anslutning %1 från enheten?"

#~ msgid "Delete"
#~ msgstr "Ta bort"

#~ msgid "Saved networks"
#~ msgstr "Sparade nätverk"

#~ msgid "Available networks"
#~ msgstr "Tillgängliga nätverk"

#~ msgid "DNS"
#~ msgstr "DNS"

#~ msgid "Wi-fi"
#~ msgstr "WIFI"
